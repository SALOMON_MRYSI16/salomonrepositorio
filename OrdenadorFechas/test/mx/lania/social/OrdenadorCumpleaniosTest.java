/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.social;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GOGS
 */
public class OrdenadorCumpleaniosTest {
    
    public OrdenadorCumpleaniosTest() {
    }
    static OrdenadorCumpleanios instance;
    @BeforeClass
    public static void setUpClass(){ 
            System.out.println("BeforeClass");
            instance = new OrdenadorCumpleanios();
    }
    
    @AfterClass
    public static void tearDownClass() {
            System.out.println("AfterClass");
    }
    
    @Before
    public void setUp() {
            System.out.println("Before");
    }
    
    @After
    public void tearDown() {
            System.out.println("After");
    }

    /**
     * Test of ordenarFechasPorCumpleanios method, of class OrdenadorCumpleanios.
     */
    @Test
    public void testOrdenarFechasConNull() { //el nombre del método es irrelevante 
        System.out.println("ordenarFechasConNull");
        List<Date> fechasNacimiento = null;
        try{
            List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento); //ejecuta el método
            fail("Se esperaba una excepcion");
        }catch(Exception ex){
        
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void testOrdenarFechasConNull2() { //el nombre del método es irrelevante 
        System.out.println("ordenarFechasConNull2");
        List<Date> fechasNacimiento = null;
        List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento); //ejecuta el método
    }

    
    @Test
    public void testOrdenarFechasListaVacia() { //el nombre del método es irrelevante 
        System.out.println("ordenarFechasListaVacia");
        List<Date> fechasNacimiento = new ArrayList<>();
        OrdenadorCumpleanios instance = new OrdenadorCumpleanios();
        List<Date> expResult = new ArrayList<>(); //variable para colocar el resultado
        List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento); //ejecuta el método
        assertEquals(expResult, result); //el resultado lo mete en la variable expResult
    }

    @Test
    public void testOrdenarFechasSencillo() { //el nombre del método es irrelevante 
        System.out.println("ordenarFechasSencillo");
        List<Date> fechasNacimiento = new ArrayList<>();
        fechasNacimiento.add(new Date(66,3,5));
        fechasNacimiento.add(new Date(86,10,6));
        fechasNacimiento.add(new Date(78,1,7));

        List<Date> expResult = new ArrayList<>(); //variable para colocar el resultado
        expResult.add(new Date(66,3,5));
        expResult.add(new Date(78,1,7));
        expResult.add(new Date(86,10,6));

        List<Date> result = instance.ordenarFechasPorCumpleanios(fechasNacimiento); //ejecuta el método
        assertEquals("Numero incorrecto de resultados", 3, result.size());
        assertEquals("Fechas en orden incorrecto", expResult, result); //el resultado lo mete en la variable expResult
    }
    
}
