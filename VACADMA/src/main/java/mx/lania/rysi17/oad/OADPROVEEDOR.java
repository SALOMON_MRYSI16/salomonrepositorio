/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.oad;

import java.util.List;
import mx.lania.rysi17.vacadma.Entidades.PROVEEDOR;

/**
 *
 * @author GOGS
 */
public interface OADPROVEEDOR {
    void nuevo(PROVEEDOR proveedor);
    void actualizar(PROVEEDOR proveedor);
    void activar(Integer idProveedor);
    void desactivar(Integer idProveedor);
    List<PROVEEDOR> buscarPorNombre(String nombre);
}
