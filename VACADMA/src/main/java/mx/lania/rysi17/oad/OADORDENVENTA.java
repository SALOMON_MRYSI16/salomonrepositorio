/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.oad;

import java.util.Date;
import java.util.List;
import mx.lania.rysi17.vacadma.Entidades.ORDENVENTA;

/**
 *
 * @author GOGS
 */
public interface OADORDENVENTA {
    void nueva(ORDENVENTA venta);
    void cancelar(Integer idVenta);
    void actualizar(ORDENVENTA venta);
    List<ORDENVENTA> bucarPorFecha(Date fecha);
}
