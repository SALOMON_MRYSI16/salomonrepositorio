/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.oad;
import java.util.List;
import mx.lania.rysi17.vacadma.Entidades.ANIMAL;
/**
 *
 * @author GOGS
 */
public interface OADANIMAL {
    void crear(ANIMAL animal);
    void actualizar(ANIMAL animal);
    void activar(Integer idAnimal);
    void desactivar(Integer idAnimal);
    List<ANIMAL> bucarPorRaza(String raza);
}
